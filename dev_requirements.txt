# Install development requirements

tox
coverage
flake8

-r requirements.txt

git+https://gitlab.com/m9s/company@mbs-6.0
